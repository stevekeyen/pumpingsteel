import { Component } from '@angular/core';


import { HomePage } from '../home/home';
import { HistoryPage } from '../history/history';
import { ProgressPage } from '../progress/progress';
import { CalenderPage } from '../calender/calender';
import { VideoPage } from '../video/video';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = HistoryPage;
  tab3Root = ProgressPage;
  tab4Root = CalenderPage;
  tab5Root = VideoPage;


  constructor() {

  }
}
